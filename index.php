<?php
	//produzione ==> nuovo lagoon
	//vecchio ==> vecchio ambiente di test
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <title>Progetto Salute</title>
    
<script>
	var linkmedico = '';
</script>
  </head>
  <body>
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img class="img-fluid" src="images/logo.png" width="120" height="120" class="d-inline-block align-top" /></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#sceglipercorso">Informazioni per i Pazienti</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?act=chi_siamo">Chi Siamo</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Agenda Elettronica--
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://test.alberodellasalute.eu">Accedi</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="index.php?act=istruzioni_agenda">Istruzioni</a>
        </div>
      </li>
     	<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Richieste di farmaci periodici al Medico
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sceglimedico">Accedi</a>
        </div>
      </li>
      </li>
     	<li class="nav-item dropdown">
        <!--a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Aggiornamento anagrafica pazienti
        </a-->
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sceglimedico">Accedi</a>
        </div>
      </li>
    </ul>
    <div class="d-none">
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    </div>
  </div>
</nav>
<?php
if (($_GET['act']=='') || ($_GET['act']=='home')) {
	include('home.php');
} else if ($_GET['act']=='chi_siamo') {
	include('chisiamo.php');
} else if ($_GET['act']=='istruzioni_agenda') {
	include('istruzioni_agenda.php');
} else if ($_GET['act']=='informazioni_generali') {
	include('informazioni_generali.php');
} else if ($_GET['act']=='accesso_ambulatorio') {
	include('accesso_ambulatorio.php');
} else if ($_GET['act']=='gestione_telefonica') {
	include('gestione_telefonica.php');
}


?>

<div class="modal fade" id="richiestapassword" tabindex="-1" role="dialog" aria-labelledby="richiestapassword" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Inserisci password:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	      <form onsubmit="verificapassword();" action="javascript: void(0);">
<p>Per proseguire inserisci la password che hai ricevuto dalla segreteria:</p>
<input type="password" id="campopassword" class="form-control"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
        <button type="button" role="submit" class="btn btn-primary" onclick="verificapassword();">Prosegui</button>
      </div>
    </form>
    </div>
  </div>
</div>

<div class="modal fade" id="sceglimedico" tabindex="-1" role="dialog" aria-labelledby="sceglimedico" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Scegli il Medico per proseguire:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	      <!--a role="button" class="btn btn-success btn-block">Dott. Benvin -momentaneamente inattivo-</a-->
	       <a onclick="vaialinkmedico('https://drive.google.com/open?id=1k2ToOHDarwhfJQMWF2BqjhUpwt1vIFpBUdfZL4xi0bU');" href="#" role="button" class="btn btn-success btn-block">Dott. Benvin</a>
  	      <a onclick="vaialinkmedico('http://bit.ly/2EqwgVH');" href="#" role="button" class="btn btn-success btn-block">Dott. De Marchi</a>
   	      <a onclick="vaialinkmedico('http://bit.ly/2AJxpW5');" href="#" role="button" class="btn btn-success btn-block">Dott. Prete</a>
	      <a href="https://lagoon.progettosalute.life/" role="button" class="btn btn-success btn-block" target="_blank">Dott. Scarpa</a>
          <!--a role="button" class="btn btn-success btn-block" target="_blank">Dott. Scarpa assente fino al 12 nov 2019</a-->
	      <a onclick="vaialinkmedico('http://bit.ly/2CTFXyF');" href="#" role="button" class="btn btn-success btn-block">Dott. Schirripa</a>
          <!--a onclick="fa(true);" href="#" role="button" class="btn btn-success btn-block">prova</a-->
          <!--a onclick="vaialinkmedico('https://80.211.114.153:8443/lagoon/login');" href="#" role="button" class="btn btn-success btn-block"> Sito sperimentale -solo autorizzati- </a-->
          <a onclick="vaialinkmedico('https://80.211.60.48:8443/lagoon')" href="#" role="button" class="btn btn-success btn-block"> Sito sperimentale -solo utenti autorizzati- </a>
           
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="sceglipercorso" tabindex="-1" role="dialog" aria-labelledby="sceglipercorso" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Scegli l'area di informazione:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	      <a href="index.php?act=informazioni_generali" role="button" class="btn btn-danger btn-block" target="_self">INFORMAZIONI GENERALI</a>
	      <a href="index.php?act=accesso_ambulatorio" role="button" class="btn btn-danger btn-block" target="_self">MODALITÀ DI ACCESSO ALL'AMBULATORIO</a>
	      <a href="index.php?act=gestione_telefonica" role="button" class="btn btn-danger btn-block" target="_self">COMPORTAMENTO IN CASO DI...</a>
      </div>
    </div>
  </div>
</div>

<script>


	var MD5 = function(d){result = M(V(Y(X(d),8*d.length)));return result.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}

function verificapassword(){
	var password = $('#campopassword').val();
	var par = password;
	console.log(par);
	if (MD5(password) == '664a6ce41022b4d79f926cf89eed8ccd') {
		$('#richiestapassword').modal('hide');
		$('#sceglimedico').modal('hide');
		window.open(linkmedico);
		linkmedico = '';
		$('#campopassword').val('');
		//console.log(MD5('lagoon'));
		
	} else if (par==="medico5"){
		lanciaSito("https://docs.google.com/spreadsheets/d/1-MN6VOw7eMawbdjkqbpL7_zDhTHxBuP7AVQegBn6DAY/edit?usp=sharing");
	}
	
		else if (par==="anagrafe"){
	    alert("crea il file con Atlas->connect->fogli excel->pazienti attivi");
		lanciaSito("https://80.211.114.153:8443/lagoon/convertForm");
	
	}
	else if (par==="produzione"){
	
		lanciaSito("https://80.211.60.48:8443/lagoon/login");
	
	} else if (par==="vecchio"){
	
		lanciaSito("https://80.211.114.153:8443/lagoon/login");
	
	} else if (par==="medico4") {

       	bootbox.alert("medico41 per pannello riepilogativo");
        lanciaSito("https://docs.google.com/spreadsheets/d/1XvZ2mfJkOTcVR496Y-7u4VjLsiTvaGbglarjLYZXIfo/edit#gid=1795173784");
		
		//lancia pannello con avviso cambio modalità
		//showClassElement("nuova_richiesta",0);
	} else if (par==="medico41") {
       	
		lanciaSito("https://docs.google.com/spreadsheets/d/1nH-nISi5BxCHPuC5B12EA1uXWBRHF7TLswoJ-3H1jPI/edit?usp=sharing");
        
	} else if (par==="medico3") {
        
		lanciaSito("https://docs.google.com/spreadsheets/d/1RE1lb1w475dbi4avFTiLajHt_BJYLFvnZbMhPT1sae0/edit?usp=sharing");
	} else if (par==="medico2") {
		lanciaSito("https://docs.google.com/spreadsheets/d/1GJ6IVL5AqGq5Jo7rd8p6s8EkI6BjRF6AI9ZCtQp5r2c/edit?usp=sharing");
	} else if (par==="medico1") {
		lanciaSito("https://docs.google.com/spreadsheets/d/1oiMJBF4csQyLA2084_KnQl58WMQsE4cVu6sCOWqO7EM/edit?usp=sharing");
	} else if (MD5(password) != '664a6ce41022b4d79f926cf89eed8ccd') {
		$('#campopassword').val('');
		$('#richiestapassword').modal('hide');
		bootbox.alert('la password inserita è errata.');
	}

}

function vaialinkmedico(url){

	$('#sceglimedico').modal('hide');
	$('#richiestapassword').modal('show'); 
	linkmedico = url;
	//window.open(url);
}


function lanciaSito(sito) {
	//loc = document.getElementById("a1");
	//loc.setAttribute('href',sito);
	window.open(sito,"_blank");
}

</script>


<?php
	include('footer.php');
	?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<script src="js/bootbox.min.js"></script>
  </body>
</html>
<?php include('log.php');
	?>
	