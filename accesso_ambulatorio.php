<div class="row">
	<div class="col-md-12">
		<div class="card mh-100">
			<div class="card-body">
<p class="text-center">Informazioni per l'accesso all'ambulatorio:</p>
<p class="card-text">
	-In caso di contatto anche solo sospetto con soggetto portatore di covid19, <b style="color: red">non presentarsi in ambulatorio </b>ma contattare la 
	telefonicamente la segreteria.<br><br>
	-Allo scopo di limitare il contagio, tutti i pazienti con problemi banali o differibili sono caldamente invitati a non presentarsi
	in ambulatorio per una visita<br><br>
	-L'accesso alla sala d'aspetto è consentito solo poco prima di entrare per la visita, nel frattempo si è pregati di attendere il 
	proprio turno fuori dalla porta fino alla chiamata della segreteria;<br><br>
	-Le persone con febbre e/o tosse sono tenute ad informare la segretarie e a indossare la mascherina;<br><br>
	-Richiedere i farmaci preferibilmente tramite il sito progettosalute.life;<br><br>
	-Per chi non avesse ancora fatto, rilasciare il consenso all' FSE (Fascicolo Sanitario Elettronico) presso la segreteria;<br><br>
	-Seguire le norme generali impartite dalle autorità sanitarie (evitare i luoghi affollati e le strette di mano, lavarsi spesso le mani, ecc.)
</p>
			</div>
		</div>
	</div>
</div>

<?php
?>
