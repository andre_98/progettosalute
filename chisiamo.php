<div class="row">
	<div class="col-md-12">
		<div class="card mh-100">
			<div class="card-body">
<p class="text-center"><img style="max-width:30%;" src="images/tree-2730787_640.png"/></p>
<p class="card-text"><strong>Progetto Salute</strong> è un'associazione che intende riunire intorno allo stesso tavolo gli operatori e i fruitori dei servizi alla salute. Lo scopo dell'associazione è di <strong>favorire il dialogo e la conseguente attuazione di servizi socio-sanitari a costi sostenibili</strong>, integrando dove necessario le prestazioni del SSN.<br>
						Progetto Salute, riunisce nel suo ambito rappresentanti del Tribunale del malato, dei farmacisti, dell'impresa sociale e della medicina convenzionata e accreditata.<br><strong>Progetto Salute intende dialogare con tutte le istituzioni</strong>: in primo luogo con il <strong>Comune di Venezia</strong> con il quale ha realizzato l'<strong>Agenda Elettronica</strong> e col il quale ha in cantiere altre iniziative; auspica tuttavia di trovare attenzione anche presso L'<strong>ASL</strong> e le <strong>associazioni di pazienti, di pensionati e di tutti gli enti che a vario titolo operano nel settore della salute pubblica.</strong><br><br>
						Il sito è in fase di realizzazione; al momento intendiamo diffondere e rendere facilmente fruibili i primi due servizi operativi: l'<strong>Agenda Elettronica</strong> e <strong>la Segreteria</strong> per la prenotazione dei farmaci abituali. </p>
			</div>
		</div>
	</div>
</div>

<?php
	include('carousel.php');
?>
