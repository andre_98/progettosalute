<h1>Come iscriversi</h1>
<p>L'<strong>Agenda Elettronica (AE)</strong> per la quale lei chiede la registrazione, è il frutto di una solida collaborazione fra l'<strong>Amministrazione della Municipalità di Favaro, l'Ufficio Programmazione Sanitaria del Comune di Venezia, alcune collaboratrici di studio e alcuni medici di famiglia.</strong></br></br>

Il progetto è stato estremamente laborioso ed è tutt'ora in fase di perfezionamento: anzi, auspicabilmente le modifiche e i miglioramenti saranno costanti. Per questo motivo, periodicamente le verranno inviate delle notizie a mezzo e-mail per renderla partecipe degli aggiornamenti.</br></br>

Come si comprende bene, AE rappresenta una sfida nella quale <strong>ciascun utente è coinvolto e corresponsabilizzato</strong>: essa permette di <strong>prenotare via web gli appuntamenti dal suo medico di famiglia</strong>. Se da un lato questo facilita l'accesso al suo medico di famiglia, dall'altra impone delle cautele affinché non ci siano abusi quali per esempio un numero sconsiderato di prenotazioni. Per questo scopo, al momento, <strong>non sarà possibile prenotare più di una visita alla settimana per ciascun paziente</strong>. Non è al momento disponibile una lista di comportamenti impropri da parte del delegato, non appena sarà possibile verrà pubblicata; nel frattempo, si raccomanda caldamente l'utilizzo di ovvie norme di buon comportamento. Si precisa, che in caso di gravi violazioni al buon funzionamento di AE, le attività correlate al profilo Delegato potrebbero venire sospese.</br></br>

<strong>Questa è la videata che dovrebbe comparire:</strong></br></br>
<img src="img/img_guida_2.png" width="100%" height="auto" alt=""/></br></br>

<strong>La freccia indica il pulsante per accedere alla sezione per i nuovi utenti:</strong></br></br>
<img src="img/img_guida_1.png" width="100%" height="auto" alt=""/></br></br>

Una volta effettuata l'iscrizione al sito, le sarà inviata una mail con la quale verrà perfezionata l'iscrizione. Una volta iscritto, torni dal suo medico - <strong>si ricordi il suo nome utente (o username)!</strong> - con la lista delle persone dalle quali ha ricevuto l'incarico di prenotare.</br></br>

<strong>Per qualsiasi problema non esiti a richiedere informazioni presso la segreteria del proprio medico.</strong></br></br></br>

<a href="index.php" class="btn btn-success btn-block">Torna alla Home</a>
</br></br></br></br>